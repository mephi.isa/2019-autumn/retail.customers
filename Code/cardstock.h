#include <stock.h>
#include <string>
#ifndef CARDSTOCK_H
#define CARDSTOCK_H

class CardStock : public Stock
{
private:
    double delta;
    double discount;
    double stepOfpercent;

public:
    CardStock();
    CardStock(double delta, double discount, double stepOfpercent, date_ start, date_ end)
    {
        if(start > end)
            throw std::logic_error("Start date greater then end date");
        this->delta = delta;
        this->discount = discount;
        this->stepOfpercent = stepOfpercent;
        this->PlanStartDate = start;
        this->PlanEndDate = end;
    }
    void apply(Check item[]) override;
};

#endif // CARDSTOCK_H
