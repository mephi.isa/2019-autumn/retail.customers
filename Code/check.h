#include <iostream>
#include <memory>
#include <string>
#ifndef CHECKITEM_H
#define CHECKITEM_H
using namespace std;
struct Items
{
    string product;        // id продукта
    int amount;            // кол-во продукта
    double price;          // цена продукта
    double discount = 0.0; // скидка на продукт
    double totalPrice;     // итоговая стоимость покупки
};
struct Check
{
    unsigned int SIZE;       // кол-во покупок в чеке
    double totalPrice = 0.0; // сумма чека
    bool haveCard;
    Items *Items = nullptr;
    shared_ptr<struct Items> ptr{Items}; // нужен для интеллектуального управления памятью
    void show(); // вывести данные чека и данные его содержимого
    void refresh(); // обновить данные чека
    Check();
    Check(string product, int amount, double price, bool haveCard, unsigned int itemsCount)
    {
        Items = new struct Items[itemsCount];
        this->Items->product = product;
        this->Items->amount = amount;
        this->Items->price = price;
        this->Items->totalPrice = price * amount;
        this->SIZE = itemsCount;
        this->haveCard = haveCard;
        unsigned int j = 0;
        for (auto i = this->Items; j < SIZE; i++, j++) {
            totalPrice += i->totalPrice;
        }
    }
    ~Check() {}
};

#endif // CHECKITEM_H
