#include <check.h>
void Check::show()
{
    int j = 0;
    for (auto i = this->Items; j < this->SIZE; i++, j++) {
        cout << "\nProduct: " << i->product << "\nPrice: " << i->price << "\nAmount: " << i->amount
             << "\nDiscount: " << i->discount * 100 << "%"
             << "\nTotal price: " << i->totalPrice << endl;
    }
    cout << "\nTotal price all product: " << this->totalPrice << endl;
}

void Check::refresh()
{
    int j = 0;
    this->totalPrice = 0;
    for (auto i = this->Items; j < this->SIZE; i++, j++) {
        i->totalPrice = i->price * i->amount;
        this->totalPrice += i->totalPrice;
    }
}
