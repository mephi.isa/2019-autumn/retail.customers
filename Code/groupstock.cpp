#include "groupstock.h"

GroupStock::GroupStock() {}

void GroupStock::apply(Check item[])
{
    if(item->SIZE < this->product.size())
        throw std::logic_error("Count of product in the check less then count of product in the stock");
    int j = 0;
    int d = 0;
    int countOfMatch = 0;
    int minimalCount = 0;
    /* ищем соответствия в чеке и в скидке */
    for (auto i = item->Items; d < item->SIZE; i++, d++)
    {
       int lastminimal = 0;
        for (size_t k = 0; k < this->product.size(); k++)
        {
             if(i->product == this->product.at(k))
             {
                countOfMatch++;
                if(minimalCount==0)
                    minimalCount = i->amount;
                else if(i->amount < minimalCount)
                    minimalCount = i->amount;
             }
        }
    }
    if(countOfMatch == this->product.size()) {
    for (auto i = item->Items; j < item->SIZE; i++, j++) {

        for (size_t k = 0; k < this->product.size(); k++)
        {
             if(i->product == this->product.at(k))
             {
                 double discount = i->price * minimalCount;
                 discount = discount * this->discount;
                 i->totalPrice -= discount;
                 i->discount = this->discount;
                 item->totalPrice -= discount;
             }
        }

        }
    }
    else
        throw std::logic_error("Products is not in Check");
}

