#include <stock.h>
#include <string>
#ifndef GROUPSTOCK_H
#define GROUPSTOCK_H

class GroupStock : public Stock
{
private:
    std::vector<std::string> product;
    double discount;

public:
    GroupStock();
    GroupStock(std::vector<std::string> product, double discount, date_ start, date_ end)
    {
        if(start > end)
            throw std::logic_error("Start date greater then end date");
        this->product = product;
        this->discount = discount;
        this->PlanStartDate = start;
        this->PlanEndDate = end;
    }
    void apply(Check item[]) override;
    ~GroupStock() {}
};

#endif // GROUPSTOCK_H
