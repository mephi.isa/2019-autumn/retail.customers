#include "productstock.h"

ProductStock::ProductStock()
{

}

void ProductStock::apply(Check item[])
{
    int j = 0;
    for (auto i = item->Items; j < item->SIZE; i++, j++) {
        if (i->product == this->product) {
            cout << "\nApply product discount for product " << i->product + "..." << endl;
            double discount = (i->totalPrice) * this->discount; // скидка (не в %)
            i->totalPrice -= discount;
            i->discount = this->discount;
            item->totalPrice -= discount;
        } else
            cout << "\nProduct discount for " << this->product + " can't used for "
                 << i->product + " product" << endl;
    }
}
