#include <stock.h>
#include <string>
#ifndef PRODUCTSTOCK_H
#define PRODUCTSTOCK_H

class ProductStock : public Stock
{
private:
    std::string product;
    double discount;

public:
    ProductStock();
    ProductStock(std::string product, double discount, date_ start, date_ end)
    {
        if(start > end)
            throw std::logic_error("Start date greater then end date");
        this->product = product;
        this->discount = discount;
        this->PlanStartDate = start;
        this->PlanEndDate = end;
    }
    void apply(Check item[]) override;
    ~ProductStock() {}
};

#endif // PRODUCTSTOCK_H
