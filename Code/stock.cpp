#include <stock.h>

using namespace date;
using namespace std::chrono;
void Stock::approve()
{
    if(this->isApproved_)
        throw std::logic_error("The stock is already approved");
    this->isApproved_ = true;
}

bool Stock::isActive()
{
    auto today = year_month_day{floor<days>(system_clock::now())};
    if (this->isFinished_)
        return false;
    if (this->isActive_)
        return true;
    if ( (today <= this->PlanEndDate && today >= this->PlanStartDate) || (today <= this->FactEndDate && today >= this->FactStartDate))
    {
        return true;
    }
    else
    {
        return false;
    }

}

bool Stock::isFinished()
{
    auto today = year_month_day{floor<days>(system_clock::now())};
    if(this->isFinished_)
        return true;
    if(today > this->PlanEndDate)
        return true;
    return false;
}
bool Stock::isApproved()
{
    return this->isApproved_;
}

void Stock::manuallyStartStock()
{
    auto today = year_month_day{floor<days>(system_clock::now())};
    this->FactStartDate = today;
    if(this->isActive())
        throw std::logic_error("Can't start the stock. The stock is now activated");
   else if(this->isFinished())
        throw std::logic_error("Can't start the stock. The stock is already finished");
    this->isManuallyStarted_ = true;
    this->isActive_ = true;
}

void Stock::manuallyEndStock()
{
    auto today = year_month_day{floor<days>(system_clock::now())};
    this->FactEndDate = today;
    if(this->isFinished())
        throw std::logic_error("Can't finish the stock. The stock is already finished");
    this->isManuallyStoped_ = true;
    this->isFinished_ = true;
    this->isActive_ = false;
}

bool Stock::isManuallyStarted()
{
    return this->isManuallyStarted_;
}

bool Stock::isManuallyStoped()
{
    return this->isManuallyStoped_;
}
