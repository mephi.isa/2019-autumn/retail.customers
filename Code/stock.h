#include <check.h>
#include <date/date.h>
#include <string>
#ifndef STOCK_H
#define STOCK_H

typedef date::year_month_day date_;
class Stock
{
protected:
    date_ FactStartDate;
    date_ FactEndDate;
    date_ PlanStartDate;
    date_ PlanEndDate;
    bool isApproved_ = false;
    bool isManuallyStarted_ = false;
    bool isManuallyStoped_ = false;
    bool isActive_ = false;
    bool isFinished_ = false;

public:
    virtual void apply(Check item[]) = 0;
    void approve();
    bool isActive();
    bool isFinished();
    bool isApproved();
    void manuallyStartStock();
    void manuallyEndStock();
    bool isManuallyStarted();
    bool isManuallyStoped();
};

#endif // STOCK_H
