#ifndef TST_TEST_H
#define TST_TEST_H
#include <cardstock.h>
#include <check.h>
#include <gmock/gmock-matchers.h>
#include <groupstock.h>
#include <gtest/gtest.h>
#include <productstock.h>
#include <stock.h>
#include <date/date.h>
#include <vector>
#include <string>
using namespace date;
using namespace testing;



TEST(CheckInitializeItemTest, PositiveTest)
{
    /*Тест проверяет конструктор класса Check*/
    Check item("Potato", 2, 440.0, true, 1);
    ASSERT_TRUE(item.Items->product == "Potato");
    ASSERT_TRUE(item.Items->amount == 2);
    ASSERT_TRUE(item.Items->price == 440.0);
    ASSERT_TRUE(item.haveCard == true);
    ASSERT_TRUE(item.SIZE == 1);
}

TEST(CheckInitializeItemTest, NegativeTest)
{
    /*Тест проверяет конструктор класса Check*/
    Check item("Potato1", 21, 441.0, true, 1);
    ASSERT_FALSE(item.Items->product == "Potato");
    ASSERT_FALSE(item.Items->amount == 2);
    ASSERT_FALSE(item.Items->price == 440.0);
    ASSERT_FALSE(item.haveCard == false);
    ASSERT_FALSE(item.SIZE == 2);
}

TEST(ProductStockTest, PositiveTest)
{
    /*Тест проверяет скидку на один товар*/
    Check item("Pumpkin", 2, 335.0, false, 1);
    ProductStock stock{"Pumpkin", 0.2, 2019_y/March/22, 2019_y/March/23};
    stock.apply(&item);
    ASSERT_TRUE(item.totalPrice == 536);
}

TEST(ProductStockTest, NegativeTest)
{
    /*Тест проверяет скидку на один товар*/
    Check item("Pumpkin", 2, 335.0, false, 1);
    ProductStock stock("Pumpkin1", 0.15, 2019_y/March/22, 2019_y/March/23);
    stock.apply(&item);
    ASSERT_FALSE(item.totalPrice == 569.5);
}

TEST(GroupStock, PositiveTest)
{
    /*Тест проверяет скидку на группу товаров, когда скидочные товары есть в чеке*/
    Check item("Pumpkin", 2, 335.0, false, 3);
    item.Items[1] = {"Cherry", 20, 10.0};
    item.Items[2] = {"Potato",20,10.0};
    item.refresh();
    std::vector <std::string> products;
    products.push_back("Pumpkin");
    products.push_back("Cherry");
    GroupStock stock(products, 0.25, 2019_y/March/22, 2019_y/March/23);
    EXPECT_NO_THROW(stock.apply(&item));
    ASSERT_TRUE(item.Items[0].totalPrice == 502.5);
    ASSERT_TRUE(item.Items[1].totalPrice == 195.0);
    ASSERT_TRUE(item.Items[2].totalPrice == 200.0);
    ASSERT_TRUE(item.totalPrice == 897.5);
}

TEST(GroupStock, NegativeTest)
{
    /*Тест проверяет скидку на группу товаров, когда в чеке нет нужных товаров*/
    Check item("Pumpkin", 2, 335.0, false, 2);
    item.Items[1] = {"Giga Cherry", 20, 10.0};
    item.refresh();
    std::vector <std::string> products;
    products.push_back("Pumpkin");
    products.push_back("Cherry");
    GroupStock stock(products, 0.25, 2019_y/March/22, 2019_y/March/23);
    EXPECT_THROW(stock.apply(&item),logic_error);
    ASSERT_FALSE(item.Items[0].totalPrice == 502.5);
    ASSERT_FALSE(item.Items[1].totalPrice == 150.0);
    ASSERT_FALSE(item.totalPrice == 652.5);
}
TEST(GroupStock, NegativeTest1)
{
    /*Тест проверяет скидку на группу товаров, когда в чеке товаров меньше, чем в акции*/
    Check item("Pumpkin", 2, 335.0, false, 1);
    item.refresh();
    std::vector <std::string> products;
    products.push_back("Pumpkin");
    products.push_back("Cherry");
    GroupStock stock(products, 0.25, 2019_y/March/22, 2019_y/March/23);
    EXPECT_THROW(stock.apply(&item),logic_error);
    ASSERT_FALSE(item.Items[0].totalPrice == 502.5);
    ASSERT_FALSE(item.totalPrice == 652.5);
}

TEST(CardStock, PositiveTest)
{
    /*Тест проверяет скидку на товар по карте*/
    Check item("Pumpkin", 2, 335.0, true, 1);
    CardStock stock(10000.0, 0.2, 0.05, 2019_y/March/22, 2019_y/March/23);
    stock.apply(&item);
    ASSERT_TRUE(item.totalPrice == 536);
}

TEST(CardStock, NegativeTest)
{
    /*Тест проверяет скидку на товар по карте*/
    Check item("Pumpkin", 2, 335.0, true, 1);
    CardStock stock(10000.0, 0.25, 0.05, 2019_y/March/22, 2019_y/March/23);
    stock.apply(&item);
    ASSERT_FALSE(item.totalPrice == 536);
}
TEST(ApproveStock, PositiveTest)
{
    /*Тест проверяет статус акции после её утверждения*/
    CardStock stock(10000.0, 0.2, 0.05, 2019_y/March/22, 2019_y/March/23);
    stock.approve();
    ASSERT_TRUE(stock.isApproved());
}
TEST(ApproveStock, NegativeTest)
{
    /*Тест проверяет утверждена ли акция*/
    CardStock stock(10000.0, 0.2, 0.05, 2019_y/March/22, 2019_y/March/23);
    stock.approve();
    EXPECT_THROW(stock.approve(),logic_error);
}
TEST(ManuallyStartStock, PositiveTest)
{
    /*Тест проверяет запуск акции до её начала*/
    CardStock stock(10000.0, 0.2, 0.05, 2020_y/12/20, 2020_y/12/31);
    stock.manuallyStartStock();
    ASSERT_TRUE(stock.isActive());
    ASSERT_FALSE(stock.isFinished());
}
TEST(ManuallyStartStock, NegativeTest)
{
    /*Тест проверяет запуск акции после её завершения*/
    CardStock stock(10000.0, 0.2, 0.05, 2019_y/March/22, 2019_y/March/23);
    EXPECT_THROW(stock.manuallyStartStock(),logic_error);
}
TEST(ManuallyEndStock, PositiveTest)
{
    /*Тест проверяет завершение акции после её окончания*/
    CardStock stock(10000.0, 0.2, 0.05, 2019_y/12/1, 2019_y/12/31);
    stock.manuallyEndStock();
    ASSERT_TRUE(stock.isFinished());
    ASSERT_FALSE(stock.isActive());
}
TEST(ManuallyEndStock, NegativeTest)
{
    /*Тест проверяет завершение акции после её окончания*/
    CardStock stock(10000.0, 0.2, 0.05, 2019_y/March/22, 2019_y/March/23);
    EXPECT_THROW(stock.manuallyEndStock(),logic_error);
}
TEST(ManuallyStartedStock, PositiveTest)
{
    /*Тест проверяет была ли запущена акция вручную*/
    CardStock stock(10000.0, 0.2, 0.05, 2019_y/December/29, 2019_y/December/31);
    stock.manuallyStartStock();
    ASSERT_TRUE(stock.isManuallyStarted());
}
TEST(ManuallyStartedStock, NegativeTest)
{
    /*Тест проверяет была ли запущена акция вручную*/
    CardStock stock(10000.0, 0.2, 0.05, 2019_y/December/29, 2019_y/December/31);
    ASSERT_FALSE(stock.isManuallyStarted());
}
TEST(ManuallyEndedStock, PositiveTest)
{
    /*Тест проверяет остановлена ли акция вручную*/
    CardStock stock(10000.0, 0.2, 0.05, 2019_y/December/3, 2019_y/December/31);
    stock.manuallyEndStock();
    ASSERT_TRUE(stock.isManuallyStoped());
}
TEST(ManuallyEndedStock, NegativeTest)
{
    /*Тест проверяет остановлена ли акция вручную*/
    CardStock stock(10000.0, 0.2, 0.05, 2019_y/March/21, 2019_y/March/23);
    ASSERT_FALSE(stock.isManuallyStoped());
}
TEST(IsActiveStock, PositiveTest)
{
    /*Тест проверяет активна ли акция в данный момент*/
    CardStock stock(10000.0, 0.2, 0.05, 2019_y/12/3, 2019_y/12/31);
    ASSERT_TRUE(stock.isActive());
}
TEST(IsActiveStock, NegativeTest)
{
    /*Тест проверяет активна ли акция в данный момент*/
    CardStock stock(10000.0, 0.2, 0.05, 2019_y/12/3, 2019_y/12/4);
    ASSERT_FALSE(stock.isActive());
}
TEST(CorrectConstructorDate, PositiveTest)
{
    /*Тест проверяет корректно ли введена дата в конструкторе акции*/
    EXPECT_NO_THROW(CardStock stock(10000.0, 0.2, 0.05, 2019_y/12/3, 2019_y/12/4));
    std::vector<std::string> products;
    EXPECT_NO_THROW(GroupStock stock1(products,0.25, 2019_y/12/2, 2019_y/12/12));
    EXPECT_NO_THROW(ProductStock stock2("Cherry",0.10, 2019_y/12/5, 2019_y/12/16));
}
TEST(CorrectConstructorDate, NegativeTest)
{
    /*Тест проверяет корректно ли введена дата в конструкторе акции*/
    EXPECT_THROW(CardStock stock(10000.0, 0.2, 0.05, 2019_y/12/8, 2019_y/12/4), logic_error);
    std::vector<std::string> products;
    EXPECT_THROW(GroupStock stock1(products,0.25, 2019_y/12/12, 2019_y/12/2), logic_error);
    EXPECT_THROW(ProductStock stock2("Cherry",0.10, 2019_y/12/16, 2019_y/12/5), logic_error);
}
#endif // TST_TEST_H
