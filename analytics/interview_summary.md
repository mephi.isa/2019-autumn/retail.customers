## Выжимка из интервью

__Какие задачи призвана решать система?__
Хотим знать, что покупают покупатели: их привычки. Вводить акции на товары в зависимости от того, как они ведут себя при совершении покупок. Система должна помогать в проведении акций, собирать информацию для разработок акций: (на какие группы товаров скидки, какого плана скидки, типы скидок, много простора для творчества). Система должна позволять сотрудникам это творчество проявлять.

__В какой форме поступают статистические данные в эту систему?__
Информация о покупках, совершенных одним человеком (по банковской карточке)

Отдел аналитики общий на все точки, все акции проводятся глобально, во всех магазинах сети.

15-20 пользователей: младшие аналитики, старшие аналитики, начальник отдела.
- Введенная акция должна либо исходить от старшего аналитика, либо должна быть им утверждена. 
- Начальник имеет право разрешать конфликты и досрочно прекращать акции. Он должен уведомляться о неэффективных акциях
- Начальник имеет доступ к результатам работы аналитиков и может оценивать их эффективность.
- Все сотрудники однородны в плане доступа к статистике покупателей.

Статистика только накапливается.
Система должна давать возможность получать данные с разными разрезами и разными продолжительностями.
Факт существоавания акций должен сохраняться, аналитика предполагается также и по акциям

__Интеграция с сервисами?__
Подумайте, что нужно из сервисом вам. 
Нужна интеграция с 
- системой, отвечающей за кадры
- системой, отправляющей данные о покупках с касс.
